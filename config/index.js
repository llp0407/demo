'use strict'
// Template version: 1.2.7
// see http://vuejs-templates.github.io/webpack for documentation.

const path = require('path')

module.exports = {
  // 开发过程中使用的配置
  dev: {
    env: require('./dev.env'),       // 环境变量
    host: 'localhost',               // 可以通过process.env.host被覆盖
    port: 9090,                      // 监听的端口
    autoOpenBrowser: true,           // 是否自动打开浏览器
    assetsSubDirectory: 'static',    //  静态资源文件夹
    assetsPublicPath: '/',           // 发布路径
    // 代理配置表，在这里可以配置特定的请求代理到对应的API接口
    // 例如将'localhost:8080/api/xxx'代理到'www.example.com/api/xxx'
    proxyTable: {},
    errorOverlay: true,
    notifyOnErrors: true,
    poll: false,
    // Use Eslint Loader?
    useEslint: true,
    // If true, eslint errors and warnings will also be shown in the error overlay
    // in the browser.
    showEslintErrorsInOverlay: false,
    // https://webpack.js.org/configuration/devtool/#development
    devtool: 'eval-source-map',
    // If you have problems debugging vue-files in devtools,
    // set this to false - it *may* help
    // https://vue-loader.vuejs.org/en/options.html#cachebusting
    cacheBusting: true,

    // CSS Sourcemaps off by default because relative paths are "buggy"
    // with this option, according to the CSS-Loader README
    // (https://github.com/webpack/css-loader#sourcemaps)
    // In our experience, they generally work as expected,
    // just be aware of this issue when enabling this option.
    cssSourceMap: false,
  },

  build: {
    env: require('./prod.env'),
    //  html入口文件
    index: path.resolve(__dirname, '../dist/index.html'),

    // 产品文件的存放路径
    assetsRoot: path.resolve(__dirname, '../dist'),
    //  二级目录，存放静态资源文件的目录，位于dist文件夹下
    assetsSubDirectory: 'static',
    assetsPublicPath: './',

    //  是否使用source-map
    productionSourceMap: true,
    // https://webpack.js.org/configuration/devtool/#production
    devtool: '#source-map',

    // Gzip off by default as many popular static hosts such as
    // Surge or Netlify already gzip all static assets for you.
    // Before setting to `true`, make sure to:
    // npm install --save-dev compression-webpack-plugin
    // 是否开启gzip压缩
    productionGzip: false,
    // gzip模式下需要压缩的文件的扩展名，设置js、css之后就只会对js和css文件进行压缩
    productionGzipExtensions: ['js', 'css'],

    // Run the build command with an extra argument to
    // View the bundle analyzer report after build finishes:
    // `npm run build --report`
    // Set to `true` or `false` to always turn it on or off
    // 是否展示webpack构建打包之后的分析报告
    bundleAnalyzerReport: process.env.npm_config_report
  }
}
