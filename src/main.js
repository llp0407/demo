// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store/index'
import Tool from './util/tool'
// 引入公共样式
import './assets/css/public.css'
import './assets/css/user.scss'
// 引入字体格式化大小文件
import './assets/js/font_size'

// 引入mint-ui库
import Mint from 'mint-ui'
import 'mint-ui/lib/style.css'

// mint-ui 样式修改
import './assets/css/mintUi.scss'
import './assets/css/toast.scss'



let open_id = Tool.getUrlParam('open_id');

localStorage.setItem('open_id', open_id);


// 获取mqqt信息，并创建mqqt服务
var obj = {
  'open_id': localStorage.getItem('open_id')
};
Tool.singleRequest("post", Tool.getMqttUrl, obj).then(res => {
  console.log(res);
  if (res) {
    localStorage.setItem("mqtthost", res.data.data.mqtthost);
    localStorage.setItem("clientid", res.data.data.clientid);
    localStorage.setItem("loginname", res.data.data.loginname);
    localStorage.setItem("password", res.data.data.password);
    localStorage.setItem("endpoint", res.data.data.endpoint);
    Tool.createWebsocketClient();
  } else {
    console.log("mqqt err");
  }
});

// 路由拦截验证
router.beforeEach((to, from, next) => {
  if (to.meta.requireAuth) {  // 判断该路由是否需要登录权限
    document.title = to.meta.title;
    Tool.singleRequest("post", Tool.getdeviceUrl, {  // 获取device id
      method: "get",
      api: "getdevicebyuser",
      para: `{
                    "open_id":"${localStorage.getItem("open_id")}"
                }`
    }).then(res => {

      if (res.data.data.devicelist.length != 0) { // 如果有设备id
        res.data.data.devicelist.forEach(ele => {
          localStorage.setItem("fog_device_id", ele.fog_device_id);
          Tool.device_id = ele.fog_device_id;
          localStorage.setItem("fog_product_id", ele.fog_product_id);
          localStorage.setItem("wx_device_id", ele.wx_device_id);
          next();   // 放行
        });
        // 获取aideviceid
        Tool.singleRequest("get", Tool.getAideviceUrl + `?fog_device_id=${localStorage.getItem("fog_device_id")}`
        ).then(res => {
          localStorage.setItem('ai_device_id', res.data.data.ai_device_id)
        });
      }
      else if (res.data.data.devicelist.length == 0) { // 没有就跳转到绑定设备页面
        document.title = to.meta.title
        next({
          path: '/getdevice'
        })
      }
    })
  }
  else {
    document.title = to.meta.title
    next();
  }
})



Vue.use(Mint);

// 设置为 false 以阻止 vue 在启动时生成生产提示。
Vue.config.productionTip = false;

var abc = new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: {App}
})

