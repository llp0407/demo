// 引入 vue  和vuex
// 前提：必须安装 vuex
import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
var state = {  // 所有的共享数据放在这个里面
  popupVisible: false,
  deviceid : '',
  isshowpop:false
}
var mutations = {   // 所有的方法放在这个里面
  showPop (state) {
    state.popupVisible = !state.popupVisible
  },
  isshow(state){
    state.isshowpop = !state.isshowpop
  }
}
export default new Vuex.Store({
  state,
  mutations
})