var serverUrl = 'https://story.zmapp.com/';
// var serverUrl = 'https://www.jx-tech.cn/';
var Config = {
  serverUrl: serverUrl,
  reqestUrl: serverUrl + 'web/aliaudioapi/',
  fogRequsetUrl: serverUrl + 'web/collectionlist/',
  fogCollectionUrl: serverUrl + 'web/collection/',
  userinfoUrl: serverUrl + 'web/babyinfo/',
  getaidevice: serverUrl + 'web/getalideviceid/',
  setdeviceUrl: serverUrl + 'web/adjustsmg/',
  getdeviceUrl: serverUrl + 'web/fogapi/',
  getqrcodeUrl: serverUrl + 'web/getqrcode/',
  getMqttUrl: serverUrl + 'web/getcommoninfo/',
  playMusicUrl: serverUrl + 'web/playlist/',
  wechat_alias: '小寻故事机',

}

export default Config
