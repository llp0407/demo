// 转化为表单方式提交
const toFormData = (data) => {
    let dataArr = []
    for (let item in data) {
      dataArr.push(`${item}=${data[item]}`)
    }
    return dataArr.join('&')
  }
  export {
    toFormData
  }
  