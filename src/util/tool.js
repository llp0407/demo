import axios from 'axios'
import Config from './config'

export default {
    client: null,
    reqUrl: Config.reqestUrl,
    fogUrl: Config.fogRequsetUrl,
    fogCollectionUrl: Config.fogCollectionUrl,
    userUrl: Config.userinfoUrl,
    setdeviceUrl: Config.setdeviceUrl,
    getdeviceUrl: Config.getdeviceUrl,
    getAideviceUrl: Config.getaidevice,
    getqrcodeUrl: Config.getqrcodeUrl,
    getMqttUrl: Config.getMqttUrl,
    playMusicUrl: Config.playMusicUrl,
    open_id: Config.open_id,
    queryName: Config.wechat_alias,
    device_id: null,
    isDeviceOnline: null,
    childlock_value: null,
    luminance_value: null,
    sn_value: null,
    version_value: null,
    volume_value: null,
    battery_value: null,
    messageobj: null,
    responeMessage:null,
    /**
     *
     * promise请求封装，返回promise对象
     * @returns
     */
    getPromise(reqObj) {
        if (typeof reqObj !== 'object') {
            return
        }
        if (reqObj.url === '') {
            throw 'please check your request param ===> no url'
        }
        return new Promise((resolve, reject) => {
            axios({
                method: reqObj.type || 'get',
                url: reqObj.url,
                data: reqObj.param || {}
            }).then(data => {
                if (data) {
                    resolve(data)
                }
            }).catch(err => {
                reject(err)
            })
        })
    },

    /**
     * 多请求，封装
     *
     * @param {any} reqObj
     * @returns
     */
    multiRequest(reqObj) {
        let promise_arr = []
        if (typeof reqObj !== 'object') {
            return
        }

        reqObj.forEach((ele) => {
            promise_arr.push(
                this.singleRequest(ele.type, ele.url, ele.params)
            )
        });

        return new Promise((resolve, reject) => {
            Promise.all(promise_arr).then(res => {
                if (res) {
                    resolve(res)
                }
            }).catch(err => {
                reject(err)
            })
        })
    },

    /**
     * 超时函数
     *
     * @param {any} ms
     */
    delayRequestFn(ms) {
        return new Promise(resolve => {
            setTimeout(() => {
                resolve({
                    msg: '请求超时，请确认你的网络正常',
                    data: {}
                })
            }, ms)
        })
    },

    /**
     * 单请求
     * @param {any} type
     * @param {any} url
     * @param {any} param
     * @param {any} ms          //设置的请求超时时间
     * @returns
     */
    singleRequest(type, url, param, ms) {
        return new Promise((resolve, reject) => {
            Promise.race([this.delayRequestFn(ms || 10000), this.getPromise({ type: type, url: url, param: param })]).then(res => {
                if (res) {
                    resolve(res)
                }
            }).catch(err => {
                reject(err)
            })
        })
    },

    /**
     *
     * 获取url参数对象
     * @param {any} str
     */
    getUrlParam(name) {
      var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
      var r = window.location.search.substr(1).match(reg);
      if (r != null) return unescape(r[2]);
      return null;
    },

  /**
       * 创建MQTT websocket服务
       *
       */
    /**
    * 创建MQTT websocket服务
    *
    */
    createWebsocketClient() {
        var that = this;
        let randNum = parseInt(Math.random(0,100)*1000)
        let host = localStorage.getItem("mqtthost"),
            port = 8884,
            cliendId = localStorage.getItem("clientid")+randNum,
            userName = localStorage.getItem("loginname"),
            password = localStorage.getItem("password")
        that.client = new Paho.MQTT.Client(host, port, cliendId);
        var options = {
            userName: userName,
            password: password,
            keepAliveInterval: 60,
            useSSL: true,
            onSuccess: onConnect,
            onFailure: onConnectError
        }
        that.client.connect(options)

        function onConnect() {
            console.log('Connect---Success')
            onSubscribe();
        }

        // 订阅
        function onSubscribe() {
            var topic = localStorage.getItem("endpoint") + "/" + localStorage.getItem('fog_product_id') + "/" + localStorage.getItem('fog_device_id') + "/status/json";
            that.client.subscribe(topic, {
                onSuccess: onPublish,        //订阅成功发布消息
                onFailure: onError           //订阅失败提示fail
            });
            var topic2 = localStorage.getItem("endpoint") + "/" + localStorage.getItem('fog_product_id') + "/" + localStorage.getItem('fog_device_id') + "/online/json";
            that.client.subscribe(topic2, {
                onSuccess: onPublish2,        //订阅成功发布消息
                onFailure: onError2           //订阅失败提示fail
            });
        }
        function onPublish() {
            console.log('onPublish sucees');
            that.onPublishFromMQTT2Device('{"commands": {"message_type": "all_request","all_value": 1}, "open_id": "openid"}')
            // that.isDeviceOnline == true
        }
        function onError() {
            console.log('onSubscribe---Error');
            // that.isDeviceOnline == false
        }
        function onPublish2() {
            console.log('online sucees');
            // that.isDeviceOnline == true
        }
        function onError2() {
            console.log('online---Error');
            // that.isDeviceOnline == false
        }
        function onConnectError() {
            console.log('onConnectError---Error');
        }
        that.client.onConnectionLost = function (responseObject) {
            console.log("onConnectionLost:" + responseObject.errorMessage);
            if (responseObject.errorCode !== 0) {
                that.createWebsocketClient();
            }
        }
        that.client.onMessageArrived = function (message) {
          try{
            console.log("type:"+typeof(message.payloadString));
            that.responeMessage = JSON.parse(message.payloadString);
            console.log(JSON.parse(message.payloadString))
            //  return  that.messageobj = JSON.parse(message.payloadString)
            var deviceObj = JSON.parse(message.payloadString)
            //111{"type":"online","data":{"online":false}}
            //22{"commands":"xxx",.....}
            var messobj = JSON.parse(message.payloadString);
            var type = messobj.type;
            if (type != undefined) {
              if (messobj.data.online == false) {
                that.isDeviceOnline = false; // 不在线
                console.log('不在线', that.isDeviceOnline)
              }
              else if (messobj.data.online == true) {
                that.isDeviceOnline = true; // 在线
                that.onPublishFromMQTT2Device('{"commands": {"message_type": "play_request","play_value": "playstate","music_index": -1,"play_mode":-1,"music_len":-1}, "open_id": "openid"}')
                console.log('在线', that.isDeviceOnline)
              }
              return false;
            }
            // console.log(deviceObj.commands.message_type == 'all_response')
            if (deviceObj.commands.message_type == 'all_response') { // 全部数据
              if (deviceObj.commands.all_value.childlock_value != null) {
                that.childlock_value = deviceObj.commands.all_value.childlock_value
              } else {
                that.childlock_value = 0;
              }
              if (deviceObj.commands.all_value.luminance_value != null) {
                that.luminance_value = deviceObj.commands.all_value.luminance_value
              } else {
                that.luminance_value = 0;
              }
              if (deviceObj.commands.all_value.sn_value != null) {
                that.sn_value = deviceObj.commands.all_value.sn_value
              } else {
                that.sn_value = 0;
              }
              if (deviceObj.commands.all_value.version_value != null) {
                that.version_value = deviceObj.commands.all_value.version_value
              } else {
                that.version_value = 0;
              }
              if (deviceObj.commands.all_value.volume_value != null) {
                that.volume_value = deviceObj.commands.all_value.volume_value
              } else {
                that.volume_value = 0;
              }
              if (deviceObj.commands.all_value.battery_value != null) {
                that.battery_value = deviceObj.commands.all_value.battery_value
              }else{
                that.battery_value =0;
              }
            }
            if (deviceObj.commands.message_type == 'volume_response') {  // 音量
              // localStorage.setItem('volume_value', deviceObj.commands.volume_value);
              that.volume_value = deviceObj.commands.volume_value
              // console.log(that.volume_value)
            }
            if (deviceObj.commands.message_type == 'battery_response') {  // 电量
              // localStorage.setItem('volume_value', deviceObj.commands.volume_value);
              that.battery_value = deviceObj.commands.battery_value
              // console.log(that.battery_value)
            }
            if (deviceObj.commands.message_type == 'luminance_response') { // 亮度
              // localStorage.setItem('luminance_value', deviceObj.commands.luminance_value);
              that.luminance_value = deviceObj.commands.luminance_value
              // console.log(that.luminance_value)
            }
            if (deviceObj.commands.message_type == 'childlock_response') {  //童锁
              // localStorage.setItem('childlock_value', deviceObj.commands.childlock_value);
              that.childlock_value = deviceObj.commands.childlock_value
              // console.log(that.childlock_value)
            }
          }catch (err){
            console.log("err:"+err.toString());
          }
        }
    },

    /**
     *
     * 推送至设备
     *
     */
    onPublishFromMQTT2Device(payload) {
        var topic = localStorage.getItem("endpoint") + "/" + localStorage.getItem('fog_product_id') + "/" + localStorage.getItem('fog_device_id') + "/command/json";
        var message = new Paho.MQTT.Message(payload);
        message.destinationName = topic;
        this.client.send(message);
    },
    /**
     *
     * 单曲播放接口封装
     *
     */
    SinglePlayToDevice(trackId, deviceId, callback) {
        console.log(trackId);
        this.singleRequest('post', this.reqUrl, {
            method: 'get',
            para: {
                "method": "taobao.ailab.aicloud.open.audio.play",
                "schema": localStorage.getItem('fog_product_id'),
                "user_id": localStorage.getItem('fog_device_id'),
                "utd_id": localStorage.getItem('fog_device_id'),
                "track_id": trackId.toString(),
                "device_id": localStorage.getItem('device_id')
            }
        }).then(res => {
            if (res) {
                if (callback && typeof callback === 'function') {
                    callback(res)
                }
            }
        })
    }
}
