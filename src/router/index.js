import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router);
export default new Router({
  mode: 'hash',
  routes: [{
    path: '/',
    redirect: '/user',
    meta: {
      title: '个人中心'
    }
  },
    {  // 首页
      path: '/',
      component: resolve => require(['../pages'], resolve),
      children: [{
        name: 'user',
        path: 'user',
        meta: {
          keepAlive: true,
          requireAuth: true,
          title: '个人中心'
        },
        component: resolve => require(['../pages/user/user.vue'], resolve)
      }, { // baby设置
        name: 'babymsg',
        path: '/babymsg',
        meta: {
          title: '宝宝信息设置',
          // requireAuth:true,
        },
        component: resolve => require(['../pages/user/babymsg.vue'], resolve)
      }, { // 家庭成员设置
        name: 'familymember',
        path: '/familymember',
        meta: {
          title: '家庭成员设置',
          // requireAuth:true,
        },
        component: resolve => require(['../pages/user/familymember.vue'], resolve)
      },
        {  // 收藏页
          name: 'collection',
          path: 'collection',
          meta: {
            title: '我的收藏'
          },
          component: resolve => require(['../pages/collection'], resolve)
        },
        {  // 收藏页
          name: 'myCollect',
          path: '/myCollect',
          meta: {
            title: '我的收藏'
          },
          component: resolve => require(['../pages/collection/myCollect'], resolve)
        },
        {  // 播放页面
          name: 'playlist',
          path: 'playlist',
          meta: {
            title: '播放列表'
          },
          component: resolve => require(['../pages/musicLlist/playlist'], resolve)
        },
        {  // 播放页面
          name: 'playone',
          path: 'playone',
          meta: {
            title: '正在播放'
          },
          component: resolve => require(['../pages/musicLlist/playone'], resolve)
        },
        {  // 播放页面
          name: 'musicLlist',
          path: 'musicLlist',
          meta: {
            title: '播放列表'
          },
          component: resolve => require(['../pages/musicLlist'], resolve)
        },
      ]
    },
    {  // 验证设备页面
      name: 'getdevice',
      path: '/getdevice',
      meta: {
        title: '绑定设备'
      },
      component: resolve => require(['../pages/user/getdevice'], resolve)
    },
    {
      path: '*',
      component: resolve => require(['../pages'], resolve)
    }]
})
